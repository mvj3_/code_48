//Broadcasts an event notification
//before sending broadcast, make sure that your application has permission to broadcast the action. Otherwise, a SecurityException: Permission denial will be thrown
Intent iBroad = new Intent();
iBroad.setAction("actionName"); //TODO Replace 'actionName' as appropriate for your action (for example, Intent.ACTION_EDIT)
iBroad.addCategory("categoryName"); //TODO Replace 'categoryName' as appropriate for your category (for example, Intent.CATEGORY_DEFAULT) 
this.sendBroadcast(iBroad);	
